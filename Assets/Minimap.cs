﻿using UnityEngine;
using System.Collections;

public class Minimap {

	Texture2D tex;
	bool dirty = true;

	public Texture texture { get { return tex; } }

	public Minimap(int width, int height) {
		tex = new Texture2D(width, height);
		tex.filterMode = FilterMode.Point;
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
				tex.SetPixel(x, y, Color.white);
		Apply();
	}

	public void Set(int x, int y, Color color) {
		tex.SetPixel(x, y, color);
		dirty = true;
	}

	public Color Get(int x, int y) {
		return tex.GetPixel(x, y);
	}

	public void Apply() {
		if(!dirty) return;
		tex.Apply();
		dirty = false;
	}

}
