﻿using UnityEngine;
using System.Collections;

public class PrimeTest : MonoBehaviour {
	public int width=8, height=8;
	public int modulus = int.MaxValue;
	public int mult1 = 1217;
	public int mult2 = 1223;
	public float delay;
	public int waitSkip = 10;

	public int distWidth=90, distHeight=90;

	public Renderer other;

	//http://math.stackexchange.com/a/1107938
	static bool coprime(long u, long v)
	{
		if (((u | v) & 1) == 0) return false;

		while ((u & 1) == 0) u >>= 1;
		if (u == 1) return true;

		do
		{
			while ((v & 1) == 0) v >>= 1;
			if (v == 1) return true;

			if (u > v) { long t = v; v = u; u = t; }
			v -= u;
		} while (v != 0);

		return false;
	}

	IEnumerator Coroutine() {
		var itex = new Minimap(width, height);
		distWidth = distHeight = (int)Mathf.Ceil(Mathf.Sqrt(modulus));
		var dtex = new Minimap(distWidth, distHeight);
		GetComponent<Renderer>().material.mainTexture = itex.texture;
		other.GetComponent<Renderer>().material.mainTexture = dtex.texture;
		var wait = new WaitForEndOfFrame();//new WaitForSeconds(delay);

		if(!coprime(mult1, mult2)) Debug.Log("!Coprime m1 m2");
		if(!coprime(mult1, modulus)) Debug.Log("!Coprime m1 %");
		if(!coprime(mult2, modulus)) Debug.Log("!Coprime m2 %");

		int i = 0;
		int j = 0;
		int skip = waitSkip;
		while(true) {
			ulong v = ((ulong)i*(ulong)mult1 + (ulong)j*(ulong)(mult2));

			if(modulus != 0)
				v %= (ulong)modulus;
			int x = (int)( v % (ulong)distWidth);
			int y = (int)( (v / (ulong)distWidth));

			if(dtex.Get(x, y) == Color.white)
				dtex.Set(x, y, Color.black);
			else
				dtex.Set(x, y, Color.red);
			dtex.Apply();
			itex.Set(i, j, (Color)new Color32((byte)(v&0xff), (byte)((v>>8)&0xff), (byte)((v>>16)&0xff), 0xff));
			itex.Apply();
			i++;
			if(i == width) {
				j++;
				i=0;
				if(j == height)
					break;
			}
			if(skip-- == 0) {
				yield return wait;
				skip = waitSkip;
			}
		}
	}

	void Start () {
		StartCoroutine(Coroutine());
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			StopAllCoroutines();
			StartCoroutine(Coroutine());
		}
	}
}
