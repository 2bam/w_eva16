﻿using UnityEngine;
using System.Collections;

public class GunPooled : MonoBehaviour {
	public Bullet bullet;
	public Transform nozzle;

	public enum ExampleExpandPolicy {
		  Expand50Percent
		, ReuseRandom
		, ReturnNull
		, ReturnDummy
	}
	public ExampleExpandPolicy exampleExpandPolicy = ExampleExpandPolicy.ReuseRandom;

	EcoPool<Bullet> _pool;

	int Bnum = 0;
	Bullet BulletFactory(Transform parent) {
		var b = GameObject.Instantiate(bullet);
		b.name += " + " + ++Bnum;
		b.transform.parent = parent;
		b.gameObject.SetActive(false);
		return b;
	}

	void Start () {
		var pooledParent = new GameObject("[Pooled]").transform;

		//The switch/enum are just for the example and by no means mandatory
		//Neither is BulletFactory, all can be right there in the factory parameter lambda.
		//It's just convenient for the example.
		switch (exampleExpandPolicy)
		{
			case ExampleExpandPolicy.Expand50Percent:
				_pool = new EcoPool<Bullet>(
					() => BulletFactory(pooledParent),
					pool => EcoPool<Bullet>.ExpandPercent(pool, 50f)
				);
				break;
			case ExampleExpandPolicy.ReuseRandom:
				_pool = new EcoPool<Bullet>(
					() => BulletFactory(pooledParent),
					pool => EcoPool<Bullet>.ReuseRandom(pool)
				);
				break;

			case ExampleExpandPolicy.ReturnNull:
				_pool = new EcoPool<Bullet>(
					() => BulletFactory(pooledParent),
					pool => null
				);
				break;
			case ExampleExpandPolicy.ReturnDummy:
				var dummy = Instantiate(bullet);
				dummy.gameObject.SetActive(false);
				_pool = new EcoPool<Bullet>(
					() => BulletFactory(pooledParent),
					pool => dummy
				);
				break;
		}
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			var b = _pool.Acquire();
			b.transform.position = nozzle.position;
		}
	}
}

