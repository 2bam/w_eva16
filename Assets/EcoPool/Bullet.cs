﻿using UnityEngine;
using System.Collections;
using System;

public class Bullet : MonoBehaviour, IPoolable {

	void Start () {
	
	}
	
	void Update () {
		transform.Rotate(Vector3.forward, Time.deltaTime * 90f);
		transform.position += Vector3.right * Time.deltaTime * 3.7f;	
	}

	//In C++ with template traits there is no need for the following to even be inside this class
	//(or even use an interface!)
	public void OnAcquire() {
		transform.rotation = Quaternion.identity;
		gameObject.SetActive(true);
	}

	public void OnRelease() {
		gameObject.SetActive(false);
	}

	public bool CanRecycle() {
		//Hard-coded condition for the example.
		return transform.position.x > 7 || !gameObject.activeSelf;
	}
}
