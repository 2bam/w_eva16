﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {
	public Bullet bullet;
	public Transform nozzle;
	 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			GameObject.Instantiate(bullet, nozzle.position, Quaternion.identity);
		}
	}
}
