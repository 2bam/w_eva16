﻿using UnityEngine;

public interface ISector {
	void Show();
	void Hide();
	void SetColor(Color c);
	void Load(int x, int y, int seed, Color color);
	void Destroy();
}

