using UnityEngine;
using Zenject;


public class SceneProxyInstaller : MonoInstaller<SceneProxyInstaller>
{
	public bool UseProxySectors = true;
	public PrimitiveType primitive;

    public override void InstallBindings()
    {
		Debug.Log("SceneProxy Injection Installer");

		Container.BindInstance(primitive).WhenInjectedInto<Sector>();

		if(UseProxySectors) {
			Container.BindFactory<ISector, Factory<ISector>>().To<ProxySector>();
			Container.BindFactory<Sector, Factory<Sector>>();
		}
		else {
			Container.BindFactory<ISector, Factory<ISector>>().To<Sector>();
		}


    }
}