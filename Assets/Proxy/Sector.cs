﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

public class Sector : ISector {
	List<GameObject> _objs;

	[Inject]
	PrimitiveType _primitive = PrimitiveType.Cube;

	public const int SEC_SIZE = 4;

	public void Load(int x, int y, int seed, Color color) {
		_objs = new List<GameObject>();
		var rnd = new System.Random(seed);
		for(int j = 0; j<SEC_SIZE; j++) {
			for(int i = 0; i<SEC_SIZE; i++) {
				if(rnd.NextDouble() < 0.5) {
					var cube = GameObject.CreatePrimitive(_primitive);
					var xf = cube.transform;
					cube.GetComponent<Renderer>().material.color = color;
					xf.position = new Vector3(x*SEC_SIZE + i, 0, y*SEC_SIZE + j);
					_objs.Add(cube);
				}
			}
		}
		Hide();
	}

	public void Show() {
		foreach(var o in _objs)
			o.SetActive(true);
	}

	public void Hide() {
		foreach(var o in _objs)
			o.SetActive(false);
	}

	public void SetColor(Color c) {
		foreach(var o in _objs)
			o.GetComponent<Renderer>().material.color = c;
	}

	public void Destroy() {
		foreach(var o in _objs)
			GameObject.Destroy(o);
	}


};

