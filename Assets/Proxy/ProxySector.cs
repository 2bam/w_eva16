﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

public class ProxySector : ISector {
	//Heavy state
	Sector _real;

	//Lightweight state
	int _seed;
	int _x, _y;
	Color _color;

	[Inject]
	Factory<Sector> _factory;

	public void Load(int x, int y, int seed, Color color) {
		_x = x;
		_y = y;
		_seed = seed;
		_color = color;
	}

	public void Show() {
		if(_real != null) return;
		_real = _factory.Create();
		_real.Load(_x, _y, _seed, _color);
		_real.Show();
	}

	public void Hide() {
		Destroy();
	}

	public void Destroy() {
		if(_real == null) return;
		_real.Destroy();
		_real = null;
	}

	public void SetColor(Color c) {
		_color = c;

		if(_real == null) return;
		_real.SetColor(c);
	}
}

