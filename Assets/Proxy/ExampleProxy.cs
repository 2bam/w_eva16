﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Zenject;

public class ExampleProxy : MonoBehaviour {
	public int radius = 2;
	public Renderer miniMap;
	public Text text;

	public int SIZE = 5;
	Minimap _miniMapTex;
	ISector[,] _hugeMap;

	[Inject]
	Factory<ISector> _factory;

	// Use this for initialization
	void Start () {

		Color[] colors = new [] {Color.white, Color.yellow, Color.cyan};
		
		_miniMapTex = new Minimap(SIZE, SIZE);

		_hugeMap = new ISector[SIZE, SIZE];
		int hsize = SIZE/2;
		for(int i = 0; i<SIZE; i++) {
			for(int j = 0; j<SIZE; j++) {
				var c = colors[Random.Range(0, colors.Length)];
				var s = _hugeMap[i, j] = _factory.Create();
				s.Load(i-hsize, j-hsize, (i*133571 + j*575441)&int.MaxValue, c);
				_miniMapTex.Set(i, j, c);
			}
		}
		_miniMapTex.Apply();
		miniMap.material.mainTexture = _miniMapTex.texture;
	}

	void Paint(int x, int y, Color c) {
		_hugeMap[x, y].SetColor(c);
		_miniMapTex.Set(x, y, c);
		_miniMapTex.Apply();
	}

	void Update () {
		text.text = string.Format("fps {0:F1} mem {1:F2}M", 1.0f / Time.smoothDeltaTime, System.GC.GetTotalMemory(false)/1024/1024);

		transform.Rotate(Vector3.up * 2 * (Input.GetAxis("Horizontal") + Input.GetAxis("Mouse X")));
		transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * 15 * Time.deltaTime);

		int x = (int)Mathf.Floor(transform.position.x / Sector.SEC_SIZE) + SIZE / 2;
		int y = (int)Mathf.Floor(transform.position.z / Sector.SEC_SIZE) + SIZE / 2;
		Debug.Log(x+";"+ y);
		for(int i = 0; i<SIZE; i++) {
			for(int j = 0; j<SIZE; j++) {
				if(Mathf.Abs(x - i) < radius && Mathf.Abs(y - j) < radius)
					_hugeMap[i, j].Show();
				else
					_hugeMap[i, j].Hide();
			}
		}

		if(0<=x && x<SIZE && 0<=y && y<SIZE && Input.GetButton("Fire1")) {
			Paint(x, y, Color.red);
		}

	}
}
